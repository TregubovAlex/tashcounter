﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TashCounterWebApi.Repositories;
using TashCounterWebApi.Loggers;


namespace Tests
{
   [TestClass]
   public class UnitTest
   {
      private MatchesRepository _matchesRepository;

      [TestInitialize]
      public void Start()
      {
         Logger.Instance().Success("Тестовая сессия начата.////////////////////////////////////////////////////////////////");
      }

      [TestMethod]
      public void Matches()
      {
         using (_matchesRepository = new MatchesRepository())
         {
            var matches = _matchesRepository.Matches();
         }
      }

      [TestCleanup]
      public void End()
      {
         Logger.Instance().Success("Тестовая сессия закончена.- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
      }
   }
}