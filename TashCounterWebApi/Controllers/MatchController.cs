﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using TashCounterWebApi.Dto;
using TashCounterWebApi.Repositories;

namespace TashCounterWebApi.Controllers
{

  [RoutePrefix("api/Match")]
  public class MatchDictionaryController : ApiController
  {
    private MatchesRepository _matchesRepository;

    [Route("GetMatches")]
    [HttpGet]
    public IEnumerable<Match> GetMatches()
    {
      using (_matchesRepository = new MatchesRepository())
      {
        return _matchesRepository.Matches();
      }
    }

    [Route("AddMatch")]
    [HttpPost]
    public void AddMatch(Match match)
    {
      if (match.DateTimeStart == DateTime.MinValue || match.Players == null)
      {
        return;
      }

      using (_matchesRepository = new MatchesRepository())
      {
        _matchesRepository.AddMatch(new Match
        {
          Id = Guid.NewGuid(),
          DateTimeStart = match.DateTimeStart,
          Players = match.Players
        });
      }
    }
  }
}