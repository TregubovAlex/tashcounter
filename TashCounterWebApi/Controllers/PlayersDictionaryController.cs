﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using TashCounterWebApi.Dto;
using TashCounterWebApi.Repositories;

namespace TashCounterWebApi.Controllers
{

  [RoutePrefix("api/PlayersDictionary")]
  public class PlayersDictionaryController : ApiController
  {
    private PlayersRepository _playersRepository;

    [Route("GetPlayers")]
    [HttpGet]
    public IEnumerable<Player> GetPlayers()
    {
      using (_playersRepository = new PlayersRepository())
      {
        return _playersRepository.Players();
      }
    }

    [Route("VerifyCreationData")]
    [HttpPost]
    public string VerifyCreationData(PlayerToVerify player)
    {
      string result = "";
      using (_playersRepository = new PlayersRepository())
      {
        if (string.IsNullOrEmpty(player.Name))
        {
          result += "Имя не может быть пустым." + Environment.NewLine;
        }
        else if (_playersRepository.Names().Contains(player.Name.ToLower()))
        {
          result += "Имя " + player.Name + " занято. Выберите другое имя." + Environment.NewLine;
        }

        if (string.IsNullOrEmpty(player.Login))
        {
          result += "Логин не может быть пустым." + Environment.NewLine;
        }
        else if (_playersRepository.Logins().Contains(player.Login.ToLower()))
        {
          result += "Логин " + player.Login + " занят. Выберите другой логин." + Environment.NewLine;
        }
      }

      if (player.Password != player.PasswordRepeat)
      {
        result += "Введённые пароли не совпадают." + Environment.NewLine;
      }

      return result;
    }

    [Route("AddPlayer")]
    [HttpPost]
    public void AddPlayer(Player player)
    {
      if (player.Name == null || player.Login == null)
      {
        return;
      }

      using (_playersRepository = new PlayersRepository())
      {
        _playersRepository.AddPlayer(new Player
        {
          Id = Guid.NewGuid(),
          Name = player.Name,
          Login = player.Login,
          Password = player.Password
        });
      }
    }

    // GET api/<controller>/5
    //public string Get(int id)
    //{
    //  return "value";
    //}

    //// PUT api/<controller>/5
    //public void Put(int id, [FromBody]string value)
    //{
    //}

    //// DELETE api/<controller>/5
    //public void Delete(int id)
    //{
    //}
  }
}