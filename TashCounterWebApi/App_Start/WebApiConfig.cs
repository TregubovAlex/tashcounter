﻿using System.Web.Http;
using TashCounterWebApi.Loggers;

namespace TashCounterWebApi
{
  public static class WebApiConfig
  {
    public static void Register(HttpConfiguration config)
    {
      // Web API configuration and services

      // Web API routes
      config.MapHttpAttributeRoutes();

      config.Routes.MapHttpRoute(
          name: "DefaultApi",
          routeTemplate: "api/{controller}/{id}",
          defaults: new { id = RouteParameter.Optional }
      );

      Logger.Instance().Success("======================================================================================================");
      Logger.Instance().Success("Сервер приложения запущен.");
    }
  }
}