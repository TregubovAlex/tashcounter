﻿import { Component, Input, OnInit } from '@angular/core';
import { ScoreType } from "../dto/dtos";

@Component({
  selector: "score",
  templateUrl: "./score.html"
})
export class ScoreComponent implements OnInit{
  @Input()
  type: ScoreType;
  symbol: string = "8";

  ngOnInit() {
    switch (this.type) {
    case ScoreType.Plus:
      this.symbol = "+";
      break;
    case ScoreType.RoundPlus:
      this.symbol = "(+)";
      break;
    case ScoreType.Minus:
      this.symbol = "-";
      break;
    case ScoreType.RoundMinus:
      this.symbol = "(-)";
      break;
    }
  }
}