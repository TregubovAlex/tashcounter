﻿import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Player, ScoreType, MatchMode } from "../dto/dtos";
import { SharedService } from "../services/sharedService";

@Component({
  selector: "playercolumn",
  templateUrl: "./playerColumn.html"
})
export class PlayerColumnComponent implements OnInit {
  @Input()
  id: string;
  @Input()
  name: string;
  @Input()
  position: number;

  @Output()
  onMoveLeft: EventEmitter<Player> = new EventEmitter<Player>();
  @Output()
  onMoveRight: EventEmitter<Player> = new EventEmitter<Player>();
  @Output()
  onRemovePlayer: EventEmitter<Player> = new EventEmitter<Player>();

  private matchMode: MatchMode = MatchMode.Creation;
  readonly creationMode: MatchMode = MatchMode.Creation;
  readonly preOnlineMode: MatchMode = MatchMode.PreOnline;
  readonly onlineMode: MatchMode = MatchMode.Online;

  private player: Player;
  private scores: ScoreType[] = [];
  private eventPlayer: Player;

  readonly plusFactor: ScoreType = ScoreType.Plus;
  readonly minusFactor: ScoreType = ScoreType.Minus;

  constructor(private sharedService: SharedService) {
    this.sharedService.matchMode.subscribe((matchMode: MatchMode) => this.matchMode = matchMode);

    this.sharedService.player.subscribe((player: Player) => {
      if (this.player.Id === player.Id) {
        this.player = player;
      }
    });

    this.sharedService.eventPlayer.subscribe((eventPlayer: Player) => this.eventPlayer = eventPlayer);

    this.sharedService.scoreToPreviousPlayer.subscribe(e => {
      if (e.previousPlayer.Id === this.id) {
        if (e.scoreType > 0) {
          this.addSpecificScore(this.plusFactor, false);
        } else {
          this.addSpecificScore(this.minusFactor, false);
        }
      }
    });
  }

  ngOnInit() {
    this.player = {
      Id: this.id,
      Name: this.name,
      Position: this.position
    }
  }

  private moveLeft() {
    this.onMoveLeft.emit(this.player);
  }

  private moveRight() {
    this.onMoveRight.emit(this.player);
  }

  private removePlayer() {
    this.onRemovePlayer.emit(this.player);
  }

  private addScore() {
    this.sharedService.eventPlayer.next(this.player);
  }

  /**
   * Добавляет плюс или округляет минус и наоборот в зависимоти от значения параметра factor.
   * @param factor указывает знак добавляемого элемента.
   * @param isManuallyAdd true указывает, что добавление инициировано на колонке этого игрока, false - ведомый игрок.
   */
  private addSpecificScore(factor: ScoreType, isManuallyAdd: boolean) {
    this.sharedService.matchMode.next(MatchMode.Online);
    this.sharedService.eventPlayer.next(null);
    if (isManuallyAdd) {
      this.sharedService.scoreAddedManually.next({ player: this.player, scoreType: ScoreType.Plus * factor });
    }
    for (var i = 0; i < this.scores.length; i++) {
      if (this.scores[i] === ScoreType.Minus * factor) {
        this.scores[i] = ScoreType.RoundMinus * factor;
        return;
      }
    }
    this.scores.push(ScoreType.Plus * factor);
  }
}