﻿export enum MainMode {
  MainMenu = 0,
  Match = 1,
  NewPlayerRegistration = 2,
  Statistics = 3
}

export enum MatchMode {
  Creation = 0,
  PreOnline = 1,
  Online = 2,
  ReadOnlyForStatistics = 3
}

export enum PlayersDictionaryMode {
  NewPlayerRegistration = 0,
  SelectionToMatch = 1,
  ReadOnlyForStatistics = 2
}

export class Player {
  Id: string;
  Name: string;
  Position: number;
}

export enum ScoreType {
  Plus = 1,
  RoundPlus = 0.001,
  Minus = -1,
  RoundMinus = -0.001
};

export class Score {
  Type: ScoreType;
  Symbol: string;
}