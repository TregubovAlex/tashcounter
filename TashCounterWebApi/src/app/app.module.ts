import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";

import { HttpModule } from "@angular/http";

import { AppComponent } from "./app.component";
import { MainMenuComponent } from "./mainMenu/mainMenu";
import { MatchComponent } from "./match/match";
import { PlayerColumnComponent } from "./playerColumn/playerColumn";
import { ScoreComponent } from "./score/score";
import { PlayersDictionaryComponent } from "./playersDictionary/playersDictionary";
import { StatisticsComponent } from "./statistics/statistics";

import { SharedService } from "./services/sharedService";
import { HttpService } from "./services/httpService";

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  declarations: [
    AppComponent,
    MainMenuComponent,
    MatchComponent,
    PlayerColumnComponent,
    ScoreComponent,
    PlayersDictionaryComponent,
    StatisticsComponent
  ],
  bootstrap: [
    AppComponent
  ],
  providers: [
    SharedService,
    HttpService
  ]
})
export class AppModule { }
