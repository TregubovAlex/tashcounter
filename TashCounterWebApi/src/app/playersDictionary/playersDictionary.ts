﻿import { Component, OnInit, Input } from "@angular/core";
import { Response } from '@angular/http';
import { HttpService } from "../services/httpService";
import { SharedService } from "../services/sharedService";
import { Player, PlayersDictionaryMode } from "../dto/dtos";

@Component({
  selector: "playersdictionary",
  templateUrl: "./playersDictionary.html"
})
export class PlayersDictionaryComponent implements OnInit {

  @Input()
  playersDictionaryMode: PlayersDictionaryMode;

  readonly selectionToMatchMode: PlayersDictionaryMode = PlayersDictionaryMode.SelectionToMatch;
  readonly newPlayerRegistrationMode: PlayersDictionaryMode = PlayersDictionaryMode.NewPlayerRegistration;
  readonly readOnlyForStatisticsMode: PlayersDictionaryMode = PlayersDictionaryMode.ReadOnlyForStatistics;

  private allPlayers: Player[] = [];
  private players: Player[] = [];
  private filterByIdExcept: string[] = [];

  private creationName: string = "";
  private creationLogin: string = "";
  private creationPassword: string = "";
  private creationPasswordRepeat: string = "";
  private verified: boolean = false;
  private verificationErrorMessage: string = "";

  constructor(
    private httpService: HttpService,
    private sharedService: SharedService) {
    this.sharedService.filterByIdExcept.subscribe((filter: string[]) => {
      debugger;
      this.filterByIdExcept = filter;
      this.players = this.allPlayers.filter(p => this.filterByIdExcept.indexOf(p.Id) === -1);
    });
  }

  ngOnInit(): void {
    this.getData();
  }

  private getData(): any {
    this.httpService.getPlayersForDictionary().subscribe((data: Response) => {
      this.allPlayers = data.json();
      this.players = this.allPlayers.filter(p => this.filterByIdExcept.indexOf(p.Id) === -1);
    });
  }

  private playerClick(player: Player) {
    if (this.playersDictionaryMode === PlayersDictionaryMode.SelectionToMatch) {
      debugger;
      this.sharedService.movePlayerToMatch.next(player);
      this.filterByIdExcept.push(player.Id);
      this.players = this.players.filter(p => this.filterByIdExcept.indexOf(p.Id) === -1);
    }
  }

  private verifyRegistrationData() {
    var playerData = {
      name: this.creationName,
      login: this.creationLogin,
      password: this.creationPassword,
      passwordRepeat: this.creationPasswordRepeat
    };
    this.httpService.verifyCreationData(playerData).subscribe((data: Response) => {
      this.verificationErrorMessage = data.json();
      if (this.verificationErrorMessage === "") {
        this.verified = true;
      } else {
        this.verified = false;
      }
    });
  }

  private registerPlayer() {
    this.verified = false;
    var playerData = {
      name: this.creationName,
      login: this.creationLogin,
      password: this.creationPassword
    };
    this.httpService.addPlayerToDictionary(playerData).subscribe(() => this.getData());
  }
}