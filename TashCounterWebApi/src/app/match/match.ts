﻿import { Component } from "@angular/core";
import { Response } from '@angular/http';
import { HttpService } from "../services/httpService";
import { SharedService } from "../services/sharedService";
import { Player, MainMode, MatchMode, PlayersDictionaryMode } from "../dto/dtos";

@Component({
  selector: "match",
  templateUrl: "./match.html"
})
export class MatchComponent {

  private matchMode: MatchMode = MatchMode.Creation;
  readonly creationMode: MatchMode = MatchMode.Creation;
  readonly preOnlineMode: MatchMode = MatchMode.PreOnline;
  readonly onlineMode: MatchMode = MatchMode.Online;
  readonly readOnlyForStatisticsMode: MatchMode = MatchMode.ReadOnlyForStatistics;

  private players: Player[] = [];

  readonly selectionToMatch: PlayersDictionaryMode = PlayersDictionaryMode.SelectionToMatch;

  constructor(
    private httpService: HttpService,
    private sharedService: SharedService) {
    this.sharedService.matchMode.subscribe((newMode: MatchMode) => {
      if (this.matchMode === MatchMode.PreOnline && newMode === MatchMode.Online) {
        this.addMatchToDb();
      }
      this.matchMode = newMode;
    });

    this.sharedService.scoreAddedManually.subscribe(e => {
      var previousPlayer: Player;
      if (e.player.Position !== 0) {
        previousPlayer = this.players.find(p => p.Position === e.player.Position - 1);
      } else {
        previousPlayer = this.players.find(p => p.Position === this.players.length - 1);
      }

      this.sharedService.scoreToPreviousPlayer.next({ previousPlayer: previousPlayer, scoreType: e.scoreType * -1 });
    });

    this.sharedService.movePlayerToMatch.subscribe((player: Player) => {
      player.Position = this.players.length;
      this.players.push(player);
    });
  }

  private moveLeft(player: Player) {
    this.changePosition(player, -1);
  }

  private moveRight(player: Player) {
    this.changePosition(player, 1);
  }

  private changePosition(player: Player, increment: number) {
    let otherPlayerPosition: number;
    if (increment === -1) { // влево
      otherPlayerPosition =
        player.Position !== 0 ?
          player.Position - 1 :
          this.players.length - 1;
    } else { // вправо
      otherPlayerPosition =
        player.Position !== this.players.length - 1 ?
          player.Position + 1 :
          0;
    }
    let optedPlayer: Player = this.players.find(p => p.Position === player.Position);
    let otherPlayer: Player = this.players.find(p => p.Position === otherPlayerPosition);

    let tempPosition: number = optedPlayer.Position;
    optedPlayer.Position = otherPlayer.Position;
    otherPlayer.Position = tempPosition;
    this.sharedService.player.next(optedPlayer);
    this.sharedService.player.next(otherPlayer);

    this.players = this.players.sort((p1, p2) => p1.Position - p2.Position);
  }

  private removePlayer(player: Player) {
    this.players = this.players.filter(p => p.Id !== player.Id);
    this.players.forEach(p => {
      if (p.Position > player.Position) {
        p.Position--;
      }
    });
    var filterByIdForPlayersDictionary = this.players.map(p => p.Id);
    this.sharedService.filterByIdExcept.next(filterByIdForPlayersDictionary);
  }

  private startMatch() {
    this.matchMode = MatchMode.PreOnline;
    this.sharedService.matchMode.next(MatchMode.PreOnline);
  }

  private toCreationMode() {
    this.matchMode = MatchMode.Creation;
    this.sharedService.matchMode.next(MatchMode.Creation);
  }

  private addMatchToDb() {
    var matchData = {
      dateTimeStart: new Date,
      players: this.players
    };
    debugger;
    this.httpService.addMatchToDb(matchData).subscribe(() => { });
  }

  private toMainMenu() {
    this.sharedService.mainMode.next(MainMode.MainMenu);
  }
}