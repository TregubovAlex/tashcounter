﻿import { Injectable } from "@angular/core";
import { Http, Headers } from "@angular/http";
import { Player, MainMode, MatchMode, PlayersDictionaryMode } from "../dto/dtos";

@Injectable()
export class HttpService {
  constructor(private http: Http) { }

  public getPlayersForDictionary() {
    return this.http.get("../api/PlayersDictionary/GetPlayers");
  }

  public verifyCreationData(playerData: { name: string, login: string, password: string, passwordRepeat: string }) {
    let body = JSON.stringify(playerData);
    let headers = new Headers({ 'Content-Type': 'application/json; charset=utf-8' });
    return this.http.post("../api/PlayersDictionary/VerifyCreationData", body, { headers: headers });
  }

  public addPlayerToDictionary(playerData: { name: string, login: string, password: string }) {
    let body = JSON.stringify(playerData);
    let headers = new Headers({ 'Content-Type': 'application/json; charset=utf-8' });
    return this.http.post("../api/PlayersDictionary/AddPlayer", body, { headers: headers });
  }

  public addMatchToDb(matchData: { dateTimeStart: Date, players: Player[] }) {
    let body = JSON.stringify(matchData);
    let headers = new Headers({ 'Content-Type': 'application/json; charset=utf-8' });
    return this.http.post("../api/Match/AddMatch", body, { headers: headers });
  }
}