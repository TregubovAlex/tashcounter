﻿import { Injectable } from "@angular/core";
import { Subject } from "rxjs/Rx";
import { Player, ScoreType, MainMode, MatchMode } from "../dto/dtos";

@Injectable()
export class SharedService {

  public player: Subject<Player> = new Subject<Player>();

  public eventPlayer: Subject<Player> = new Subject<Player>();

  public scoreAddedManually: Subject<{ player: Player, scoreType: ScoreType }> = new Subject<any>();

  public scoreToPreviousPlayer: Subject<{ previousPlayer: Player, scoreType: ScoreType }> = new Subject<any>();

  public mainMode: Subject<MainMode> = new Subject<MainMode>();

  public matchMode: Subject<MatchMode> = new Subject<MatchMode>();

  public movePlayerToMatch: Subject<Player> = new Subject<Player>();
  
  public filterByIdExcept: Subject<string[]> = new Subject<string[]>();
}