﻿import { Player } from "../dto/dtos";

export const playersMock: Player[] = [
  {
    Id: "111",
    Name: "first",
    Position: 0
  },
  {
    Id: "2222",
    Name: "second",
    Position: 1
  },
  {
    Id: "33333",
    Name: "third",
    Position: 2
  }
];