﻿import { Component } from "@angular/core";
import { MainMode, PlayersDictionaryMode } from "../dto/dtos";
import { SharedService } from "../services/sharedService";

@Component({
  selector: "mainmenu",
  templateUrl: "./mainMenu.html"
})
export class MainMenuComponent {

  private mode: MainMode = MainMode.MainMenu;
  readonly menuMode = MainMode.MainMenu;
  readonly matchMode = MainMode.Match;
  readonly newPlayerRegistrationMode = MainMode.NewPlayerRegistration;
  readonly statisticsMode = MainMode.Statistics;

  readonly newPlayerRegistration: PlayersDictionaryMode = PlayersDictionaryMode.NewPlayerRegistration;

  constructor(private sharedService: SharedService) {
    this.sharedService.mainMode.subscribe((mainMode: MainMode) => this.mode = mainMode);
  }

  private createNewMatch() {
    this.mode = MainMode.Match;
  }

  private openPlayersDictionaryForRegistration() {
    this.mode = MainMode.NewPlayerRegistration;
  }

  private openStatistics() {
    this.mode = MainMode.Statistics;
  }
}