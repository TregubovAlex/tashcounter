﻿using System;
using System.Collections.Generic;
using System.Linq;
using TashCounterWebApi.Dto;
using MySql.Data.MySqlClient;
using TashCounterWebApi.Loggers;

namespace TashCounterWebApi.Repositories
{
  public class PlayersRepository : Repository
  {
    public PlayersRepository()
      : base("PlayersRepository")
    {
    }

    /// <summary>
    /// Возвращает список игроков из БД.
    /// </summary>
    /// <returns></returns>
    public IEnumerable<Player> Players()
    {
      string query = "SELECT Id, Name, Login, Password FROM players";
      IList<Player> result = new List<Player>();

      try
      {
        MySqlCommand command = new MySqlCommand(query, Connection);
        MySqlDataReader reader = command.ExecuteReader();

        try
        {
          while (reader.Read())
          {
            result.Add(new Player
            {
              Id = Guid.Parse(reader[0].ToString()),
              Name = reader[1].ToString(),
              Login = reader[2].ToString(),
              Password = reader[3].ToString()
            });
          }
        }
        catch (Exception exception)
        {
          Logger.Instance().Error("Ошибка при попытке получения списка игроков из БД", exception);
          return null;
        }
        finally
        {
          reader.Close();
        }
      }
      catch (Exception exception)
      {
        Logger.Instance().Error("Ошибка при попытке получения списка игроков из БД", exception);
        return null;
      }

      Logger.Instance().Success("Список игроков успешно получен:" +
        Environment.NewLine +
        "\tId=" + string.Join("," + Environment.NewLine +
                              "\tId=", result.Select(p => p.Id.ToString() + " Name=" + p.Name)));
      return result;
    }

    /// <summary>
    /// Возвращает игрока из БД по его Id.
    /// </summary>
    /// <param name="id">Id игрока.</param>
    /// <returns></returns>
    public Player Player(Guid id)
    {
      string query = "SELECT Id, Name, Login, Password FROM players WHERE Id='" + id + "'";
      Player player;

      try
      {
        MySqlCommand command = new MySqlCommand(query, Connection);
        MySqlDataReader reader = command.ExecuteReader();

        try
        {
          IList<Player> result = new List<Player>();
          while (reader.Read())
          {
            result.Add(new Player
            {
              Id = Guid.Parse(reader[0].ToString()),
              Name = reader[1].ToString(),
              Login = reader[2].ToString(),
              Password = reader[3].ToString()
            });
          }
          player = result.Single();
        }
        catch (Exception exception)
        {
          Logger.Instance().Error("Ошибка при попытке получения игрока по Id из БД", exception);
          return null;
        }
        finally
        {
          reader.Close();
        }
      }
      catch (Exception exception)
      {
        Logger.Instance().Error("Ошибка при попытке получения игрока по Id из БД", exception);
        return null;
      }

      Logger.Instance().Success("Игрок успешно получен по Id: Id=" + player.Id + " Name=" + player.Name);
      return player;
    }

    /// <summary>
    /// Возвращает занятые имена игроков из БД.
    /// </summary>
    /// <returns></returns>
    public IEnumerable<string> Names()
    {
      string query = "SELECT Name FROM players";
      IList<string> result = new List<string>();

      try
      {
        MySqlCommand command = new MySqlCommand(query, Connection);
        MySqlDataReader reader = command.ExecuteReader();

        try
        {
          while (reader.Read())
          {
            result.Add(reader[0].ToString().ToLower());
          }
        }
        catch (Exception exception)
        {
          Logger.Instance().Error("Ошибка при попытке получения списка имён игроков из БД", exception);
          return null;
        }
        finally
        {
          reader.Close();
        }
      }
      catch (Exception exception)
      {
        Logger.Instance().Error("Ошибка при попытке получения списка имён игроков из БД", exception);
        return null;
      }

      Logger.Instance().Success("Список занятых имён игроков успешно получен:" +
        Environment.NewLine + "\t " + string.Join(", ", result));
      return result;
    }

    /// <summary>
    /// Возвращает занятые логины игроков из БД.
    /// </summary>
    /// <returns></returns>
    public IEnumerable<string> Logins()
    {
      string query = "SELECT Login FROM players";
      IList<string> result = new List<string>();

      try
      {
        MySqlCommand command = new MySqlCommand(query, Connection);
        MySqlDataReader reader = command.ExecuteReader();

        try
        {
          while (reader.Read())
          {
            result.Add(reader[0].ToString().ToLower());
          }
        }
        catch (Exception exception)
        {
          Logger.Instance().Error("Ошибка при попытке получения списка логинов из БД", exception);
          return null;
        }
        finally
        {
          reader.Close();
        }
      }
      catch (Exception exception)
      {
        Logger.Instance().Error("Ошибка при попытке получения списка логинов из БД", exception);
        return null;
      }

      Logger.Instance().Success("Список занятых логинов игроков успешно получен:" +
        Environment.NewLine + "\t " + string.Join(", ", result));
      return result;
    }

    /// <summary>
    /// Добавляет игрока в БД.
    /// </summary>
    /// <param name="player">Игрок для добавления.</param>
    /// <returns></returns>
    public bool AddPlayer(Player player)
    {
      string query =
      "INSERT INTO `tashcounterdb`.`players` " +
      "(`Id`, `Name`, `Login`, `Password`) VALUES ('" +
      player.Id.ToString().ToUpper() + "', '" +
      player.Name + "', '" +
      player.Login + "', '" +
      player.Password + "');";

      try
      {
        MySqlCommand command = new MySqlCommand(query, Connection);
        command.ExecuteScalar();
      }
      catch (Exception exception)
      {
        Logger.Instance().Error("Ошибка при попытке добавления нового игрока в БД", exception);
        return false;
      }

      Logger.Instance().Success("Игрок успешно добавлен в БД: Id=" + player.Id.ToString().ToUpper() + ", Name=" + player.Name);
      return true;
    }
  }
}