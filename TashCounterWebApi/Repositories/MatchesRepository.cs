﻿using System;
using System.Collections.Generic;
using System.Linq;
using TashCounterWebApi.Dto;
using MySql.Data.MySqlClient;
using TashCounterWebApi.Loggers;

namespace TashCounterWebApi.Repositories
{
  public class MatchesRepository : Repository
  {
    private PlayersRepository _playersRepository;

    public MatchesRepository()
      : base("MatchesRepository")
    {
    }

    /// <summary>
    /// Возвращает список матчей из БД.
    /// </summary>
    /// <returns></returns>
    public IEnumerable<Match> Matches()
    {
      IList<Match> result = new List<Match>();

      try
      {
        string matchQuery = "SELECT Id, DateTimeStart FROM matches";
        MySqlCommand matchCommand = new MySqlCommand(matchQuery, Connection);
        MySqlDataReader matchReader = matchCommand.ExecuteReader();
        try
        {
          while (matchReader.Read())
          {
            result.Add(new Match
            {
              Id = Guid.Parse(matchReader[0].ToString()),
              DateTimeStart = DateTime.Parse(matchReader[1].ToString()),
              Players = new List<PlayerInMatch>()
            });
          }
        }
        catch (Exception exception)
        {
          Logger.Instance().Error("Ошибка при попытке получения списка матчей из БД (до получения участников матчей)", exception);
          return null;
        }
        finally
        {
          matchReader.Close();
        }

        IList<Participant> participantsResult = new List<Participant>();
        string participantsQuery = "SELECT MatchId, PlayerId, Position FROM participants";
        MySqlCommand participantsCommand = new MySqlCommand(participantsQuery, Connection);
        MySqlDataReader participantsReader = participantsCommand.ExecuteReader();
        try
        {
          while (participantsReader.Read())
          {
            participantsResult.Add(new Participant
            {
              MatchId = Guid.Parse(participantsReader[0].ToString()),
              PlayerId = Guid.Parse(participantsReader[1].ToString()),
              Position = int.Parse(participantsReader[2].ToString())
            });
          }
        }
        catch (Exception exception)
        {
          Logger.Instance().Error("Ошибка при попытке получения списка участников матчей из БД", exception);
          return null;
        }
        finally
        {
          participantsReader.Close();
        }

        using (_playersRepository = new PlayersRepository())
        {
          foreach (Match match in result)
          {
            foreach (Participant participant in participantsResult.Where(partic => partic.MatchId == match.Id))
            {
              var player = _playersRepository.Player(participant.PlayerId);

              match.Players.Add(new PlayerInMatch
              {
                Id = player.Id,
                Name = player.Name,
                Login = player.Login,
                Password = player.Password,
                Position = participant.Position
              });
            }
          }
        }
      }
      catch (Exception exception)
      {
        Logger.Instance().Error("Ошибка при попытке получения списка матчей из БД", exception);
        return null;
      }

      Logger.Instance().Success("Список матчей успешно получен:" +
        Environment.NewLine +
        "\tId=" + string.Join("," + Environment.NewLine +
                              "\tId=", result.Select(m => m.Id.ToString()
                              + " DateTimeStart=" + m.DateTimeStart
                              + " Players=" + string.Join(", ", m.Players.Select(p => p.Name)))));
      return result;
    }

    /// <summary>
    /// Добавляет матч в БД.
    /// </summary>
    /// <param name="match">Матч для добавления.</param>
    /// <returns></returns>
    public bool AddMatch(Match match)
    {
      string addMatchQuery =
      "INSERT INTO `tashcounterdb`.`matches` " +
      "(`Id`, `DateTimeStart`) VALUES ('" +
      match.Id.ToString().ToUpper() + "', '" +
      match.DateTimeStart + "');" + Environment.NewLine;

      foreach (PlayerInMatch player in match.Players)
      {
        addMatchQuery +=
          "INSERT INTO `tashcounterdb`.`participants` " +
          "(`MatchId`, `PlayerId`, `Position`) VALUES ('" +
          match.Id.ToString().ToUpper() + "', '" +
          player.Id.ToString().ToUpper() + "', '" +
          player.Position + "');" + Environment.NewLine;
      }

      try
      {
        MySqlCommand command = new MySqlCommand(addMatchQuery, Connection);
        command.ExecuteScalar();
      }
      catch (Exception exception)
      {
        Logger.Instance().Error("Ошибка при попытке добавления нового матча в БД", exception);
        return false;
      }

      Logger.Instance().Success("Матч успешно добавлен в БД: " + Environment.NewLine
        + "\tId=" + match.Id.ToString().ToUpper()
        + ", DateTimeStart=" + match.DateTimeStart + ", Players="
        + string.Join(", ", match.Players.Select(p => p.Name)));
      return true;
    }
  }
}