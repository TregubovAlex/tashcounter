﻿using System;
using MySql.Data.MySqlClient;
using TashCounterWebApi.Loggers;

namespace TashCounterWebApi.Repositories
{
  /// <inheritdoc />
  /// <summary>
  /// Базовый класс репозиториев.
  /// </summary>
  public class Repository : IDisposable
  {
    private const string ConnectionString = "server=localhost;user=root;database=tashcounterdb;password=root;";
    protected readonly MySqlConnection Connection;
    private readonly string _repositoryName;
    
    //todo добавить кэширование

    protected Repository(string repositoryName)
    {
      _repositoryName = repositoryName;
      try
      {
        Connection = new MySqlConnection(ConnectionString);
        Connection.Open();
        Logger.Instance().Success("Соединение с БД установлено (" + _repositoryName + ")");
      }
      catch (Exception exception)
      {
        Logger.Instance().Error("Ошибка при попытке соединения с БД (" + _repositoryName + ")", exception);
      }
    }

    public void Dispose()
    {
      Connection.Close();
      Logger.Instance().Success("Соединение с БД закрыто (" + _repositoryName + ")");
    }
  }
}