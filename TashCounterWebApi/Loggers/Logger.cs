﻿using System;
using System.Configuration;
using System.IO;

namespace TashCounterWebApi.Loggers
{
  public sealed class Logger
  {
    private readonly string _path;

    private static Logger _instance;

    private Logger()
    {
      _path = ConfigurationManager.AppSettings["LogPath"];
    }

    public static Logger Instance()
    {
      if (_instance == null)
      {
        lock (typeof(Logger))
        {
          if (_instance == null)
          {
            _instance = new Logger();
          }
        }
      }

      return _instance;
    }

    /// <summary>
    /// Логирует успешное действие.
    /// </summary>
    /// <param name="message">Сообщение для помещения в лог.</param>
    public void Success(string message)
    {
      AddLineToLog("Success", message);
    }

    /// <summary>
    /// Логирует ошибку.
    /// </summary>
    /// <param name="message">Сообщение для помещения в лог.</param>
    /// <param name="exception">Возникшее исключение.</param>
    public void Error(string message, Exception exception)
    {
      Exception currentException = exception;
      string resultMessage = message + Environment.NewLine +
                             currentException.Message + Environment.NewLine +
                             currentException.StackTrace;
      int depth = 0;
      while (currentException?.InnerException != null)
      {
        depth++;
        currentException = exception.InnerException;
        resultMessage += Environment.NewLine +
                         "\tInner Exception (depth: " + depth + ")" + Environment.NewLine +
                         "\t" + currentException?.Message + Environment.NewLine +
                         currentException?.StackTrace;
      }

      AddLineToLog("ERROR", resultMessage);
    }

    /// <summary>
    /// Добавляет событие в лог.
    /// </summary>
    /// <param name="eventType"></param>
    /// <param name="message"></param>
    private void AddLineToLog(string eventType, string message)
    {
      string line =
        DateTime.Now.Date.ToShortDateString() + " | " +
        DateTime.Now.TimeOfDay + " | " +
        eventType + " | " +
        message + ".";
      File.AppendAllLines(_path, new[] { line });
    }
  }
}