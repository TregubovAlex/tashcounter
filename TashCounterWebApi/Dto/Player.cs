﻿using System;

namespace TashCounterWebApi.Dto
{
  /// <summary>
  /// Сущность "Игрок".
  /// </summary>
  public class Player
  {
    /// <summary>
    /// Уникальный идентификатор игрока.
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// Имя игрока.
    /// </summary>
    public string  Name { get; set; }

    /// <summary>
    /// Логин игрока.
    /// </summary>
    public string Login { get; set; }

    /// <summary>
    /// Пароль игрока.
    /// </summary>
    public string Password { get; set; }
  }
}