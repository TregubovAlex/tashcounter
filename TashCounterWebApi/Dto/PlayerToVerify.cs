﻿namespace TashCounterWebApi.Dto
{
  /// <inheritdoc />
  /// <summary>
  /// Dto при создании игрока.
  /// </summary>
  public class PlayerToVerify : Player
  {
    /// <summary>
    /// Повторный ввод пароля при создании игрока.
    /// </summary>
    public string PasswordRepeat { get; set; }
  }
}