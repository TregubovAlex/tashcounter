﻿namespace TashCounterWebApi.Dto
{
  /// <inheritdoc />
  /// <summary>
  /// Dto игрока в матче.
  /// </summary>
  public class PlayerInMatch : Player
  {
    /// <summary>
    /// Позиция игрока в матче.
    /// </summary>
    public int Position { get; set; }
  }
}