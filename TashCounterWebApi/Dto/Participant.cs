﻿using System;

namespace TashCounterWebApi.Dto
{
  /// <summary>
  /// Сущность "Участник матча".
  /// </summary>
  public class Participant
  {
    /// <summary>
    /// Уникальный идентификатор матча.
    /// </summary>
    public Guid MatchId { get; set; }

    /// <summary>
    /// Уникальный идентификатор игрока.
    /// </summary>
    public Guid PlayerId { get; set; }

    /// <summary>
    /// Позиция игрока в матче.
    /// </summary>
    public int Position { get; set; }
  }
}