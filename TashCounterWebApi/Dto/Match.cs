﻿using System;
using System.Collections.Generic;

namespace TashCounterWebApi.Dto
{
  /// <summary>
  /// Сущность "Матч".
  /// </summary>
  public class Match
  {
    /// <summary>
    /// Уникальный идентификатор матча.
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// Дата и время начала матча.
    /// </summary>
    public DateTime DateTimeStart{ get; set; }

    /// <summary>
    /// Игроки матча.
    /// </summary>
    public List<PlayerInMatch> Players { get; set; }
  }
}